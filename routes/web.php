<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//For User Authentication:
// php artisan make:auth
// Authentication scaffolding generated successfully


//php artisan down - Maintenance mode 503 Service Unavailable
// php artisan up - to make live

//how to make middleware 
// php artisan make:middleware TestMiddleware - ater creating have to register controller or route
// register in kernal.php 

//URL Helpers
//action="{{url('/contact')}}" it gives complete path http://127.0.0.1
// and one is name('contact.create')


//Php artisan route:list


//NPM , Node , Vue , Webpack
/*
npm -v
node -v
npm install
webpack is a module bundler. Its main purpose is to bundle JavaScript files for usage in a browser, yet it is also capable of transforming, bundling, or packaging .

Webpack is a module bundler that takes your development assets (like Vue components and plain Javascript files) and combines them into so called bundles. This is different from simply concatenating and minifying JavaScript files. Simply put, with webpack bundles only the modules required on the current page are loaded.

npm run dev

npm run watch // if go any issue delete node modules folder and re install 
rm -rf node_modules/ && npm install


 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.

 Frontend Presets for React, Vue, Bootstrap & Tailwind CSS

 preset is swap the front-end scaffolding for the application
 none , bootstrap, vue , react
 php artisan preset react
 npm install && npm run dev
 php artisan preset none it will remove scaffolding

 In resource folder check js/components(Example.js) & js/app.js & js/bootstrap.js

 composer require laravel-frontend-presets/tailwindcss
    npm install && npm run dev && npm run dev

    Events & listeners:

 php artisan make:mail WelcomeNewUserEmail --markdown=emails.new-welcome
 

*/

Route::get('/', function () {
    return view('home');//welcome
});
// Route::get('/contact', function () {
//     return view('contact');
// });
// Route::view('/contact', 'contact');
Route::get('/contact-us', 'ContactFormController@create')->name('contact.create');
Route::post('/contact-us', 'ContactFormController@store')->name('contact.store');;
Route::get('/about', function () {
    // return 'About Us';
    return view('about');
})->middleware('test');

/* Route::get('/customers','CustomersController@index'
    // $customers = ["uday","Kumar","Reddy"];
    // return view('internals.customers',['customers'=>$customers]);
); */


/* Route::get('/customers','CustomersController@index');
Route::get('/customers/create','CustomersController@create');
Route::post('/customers','CustomersController@store');
Route::get('/customers/{customer}','CustomersController@show');
Route::get('/customers/{customer}/edit','CustomersController@edit');
Route::patch('/customers/{customer}','CustomersController@update');
Route::delete('/customers/{customer}','CustomersController@destroy'); */
Route::resource('/customers','CustomersController');//->middleware('auth')

//To create like resource controller php artisan make:controller TestController -r
//To  make resource controller with route model binding :  php artisan make:controller Test2Controller -r -m Customer
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/home', 'HomeController@index');//->name('home')
