<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //

    //Fillable Example
    // protected $fillable = ['name','email','active'];

    //Guarded is opposite of fillable
    protected $guarded = [];//'name' means name will not allow for mass assignment


    // Undefined index:
    protected $attributes = [
        'active'=>1//Default be Active in Add form of Customer Active Secletion Drop down
    ];

    public function scopeActive($query){
        return $query->where('active',1);
    }

    public function scopeInactive($query){
        return $query->where('active',0);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function getActiveAttribute($attribute){
        return $this->activeOptions()[$attribute];
    }

    public function activeOptions(){
       return  [
           1 =>'Active',
           0 =>'Inactive',
           2 =>'In-Progress'
       ];
    }
}
