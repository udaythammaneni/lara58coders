<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Company;

class AddCompanyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'command:name';
    // protected $signature = 'contact:company {name} {phone?}';// ? means optional
    protected $signature = 'contact:company {name} {phone=N/A}';// ? means optional

    /**
     * The console command description.
     *
     * @var string
     */
    // protected $description = 'Command description';
    protected $description = 'Adds A new Company';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $company = Company::create([
            'name'=>$this->argument('name'),
            // 'phone'=>'123-123-1234'
            // 'phone'=>$this->argument('phone') ?? 'N/A',
            'phone'=>$this->argument('phone'),
        ]);

        $this->info('Added Test Company'.$company->name);
        // $this->warn('This is a warning');
        // $this->error('This is an error');

        $this->ask('What is the company name ?');
        if($this->confirm('Are you ready ?')){
            
        }
    }
}
