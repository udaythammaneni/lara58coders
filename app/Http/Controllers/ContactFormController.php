<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller
{
    //

    public function create(){
        return view('contact.create');
    }

    public function store(){
        // dd(request());
        $data  = request()->validate([
            'name'=>'required',
            'email'=>'required | email',
            'message'=>'required'
        ]);
        // dd(request()->all());
        //Send Email  using Maillable
        //php artisan make:mail ContactFormMail --markdown=emails.contact.contact-form
            //so --markdown create a view template and path is email folder, contact folder, contact-form .blade.php
        //Mail trap Credentials to send email
        //Mail facades   
        // use Illuminate\Support\Facades\Mail;
        // Class 'App\Http\Controllers\ContactFormMail' not found
        // Mail::to('udaythammaneni@gmail.com')->send(new ContactFormMail());


        // Mail::to('test@test.com')->send(new ContactFormMail($data));


        // Expected response code 250 but got code "530", with message "530 5.7.1 Authentication required  - clear cache so the issue will not come...? php artisan config:cache

        // From:Example <hello@example.com> To: <test@test.com>

       /*  MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=f41b62efe69eed
MAIL_PASSWORD=9741bda073ffae
MAIL_ENCRYPTION=tls */

            // return redirect('/contact');
            // return redirect('/contact')->with('message','Thanks for your message.'); // to send messages
            session()->flash('message','Thanks for your message.');
            // return redirect('/contact'); // to send flash messages
            return redirect()->route('contact.create'); // to send flash messages


    }
}
