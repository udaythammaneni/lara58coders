<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeNewUserEmail;

use App\Events\NewCustomerHasRegisteredEvent;


use App\Customer;

use App\Company;

class CustomersController extends Controller
{

    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('auth')->only('create');
        // $this->middleware('auth')->except('index');
    }
    //Follow the resource controller pattern for method names
/*     Actions Handled By Resource Controller
Verb	URI	Action	Route Name
GET	/photos	index	photos.index
GET	/photos/create	create	photos.create
POST	/photos	store	photos.store
GET	/photos/{photo}	show	photos.show
GET	/photos/{photo}/edit	edit	photos.edit
PUT/PATCH	/photos/{photo}	update	photos.update
DELETE	/photos/{photo}	destroy	photos.destroy */
    public function index(){
        //php artisan migrate:fresh
        // $customers = ["uday","Kumar","Reddy"];
        // $customers = Customer::all();
        // dd($customers);
        

        // dd($activecustomers);
        // dd($activecustomers[0]->company->name);//Infosys
        // $inactivecustomers = Customer::where('active',0)->get();
        // $activecustomers = Customer::where('active',1)->get();
        // $inactivecustomers = Customer::inactive()->get();

        // $customers = Customer::all();
        $customers = Customer::paginate(10);

        //Eloquent ORM: It in build ORm , it providers option to intere=acts with databse very easy
        return view('customers.index',['customers'=>$customers]);
        // ['actcustomers'=>$activecustomers,'inactcustomers'=>$inactivecustomers]
    }

    public function create(){
        
        $company = Company::all();
       
        // 
        // Undefined variable: customer
         $customer = new Customer();
        return view('customers.create',['companylist'=>$company,'customer'=>$customer]);
    }

    public function store(Request $request){
            // print_r($request); Don't Use this bcz too much data it is providing (unwanted data)
            // dd(request()); dd(request('name'));
            //419 page expired CSRF
            // "_token" => "vNBJTn0JavpzTQLfDiEsRxV3YRtasO4xDrMxPyvo","name" => "hello"

            //Validate here to avoid error like SQLSTATE[23000]: Integrity constraint violation: 1048 Column 'name' cannot be null
          /*   $data = request()->validate([
                'name'=>'required | min:3',
                'email'=>'required | email',
                'active'=>'required',
                'company_id'=>'required'
                // 'random'=>''//optional field ; no validation
            ]); */
            /* $data = $request->validate([
                'name' => 'required',
            ]); */
            // Spelling Mistake In required place i gave like requred Method Illuminate\Validation\Validator::validateRequred does not exist.

                // dd($data);//name,email,active,random
                // Add [name] to fillable property to allow mass assignment on [App\Customer].

                // SQLSTATE[HY000]: General error: 1364 Field 'company_id' doesn't have a default value = Means check fillable in model
                // $customers = Customer::create($data);
                // validateRequest
                $customers = Customer::create($this->validateRequest());

                //Send A welcome Mail to the user Using Event & Listeners:
                // php artisan make:mail WelcomeNewUserEmail --markdown=emails.new-welcome


                //To make our controller code clean - we are using Events & Listeners 
                /* Mail::to($customers->email)->send(new WelcomeNewUserEmail());
                //Register to newsletters
                dump("Registered to News Letters");
                //Send Slack notification to Admin
                dump('Slack message here'); */
                //make:event  make:listeners 
                // One event & 3 listerens (Mail sending 2. Registered News Letters 3.Slack Messages)
                // print_r($customers->email);
                event( new NewCustomerHasRegisteredEvent($customers));
                //php artisan make:event NewCustomerHasRegisteredEvent
                // now create listers 
                // php artisan make:listener WelcomeNewCustomerListener
                // Mail::to($customers->email)->send(new WelcomeNewUserEmail()); This line moved to Listeners handle method
                //Now we have create connection in between Event & Listeners
                //Service providers
                dump("Registered to News Letters Customer Controller");
                //event:generate
                // php artisan event:generate  - Auto matically generates with names we provided in servvice providers

                // Queue - User don't have to wait it can run in background either image upload or in any case
                //.env QUEUE_CONNECTION = sync
                //Queues: Database Driver
                //queue:table - create a migration for the queue jobs database table
                //php artisan queue:table - it will create jobs table and migratio also
                //.env change sync to database
                //php artisan queue:work - Strat processing jobs on the queue as a daemon
                //if queue jobs not working then restart serve again
                //php artisan config:clear php artisan cache:clear
                //php artisan queue:work &  it gives process id
                //jobs 
                //jobs -l
                //KILL processID
                //php artisan queue:work > storage/logs/jobs.log &



                








                // $customers = Customer::create(request()->all());//Not good practice
                

           /*  $customers = new Customer();
            $customers->name = $request->name;
            $customers->email = request('email');
            $customers->active = request('active');
            $customers->save(); */
            // return back();

            // return redirect('/customers');

            //Now i want to add email filed in customers 
            // so In migration added $table->string('email'); php artisan migrate:rollback ; php artisan migrate
    }   


    public function show(Customer $customer){

        // $customer has to match in the route file & controller 


        // dd($customer);
        // $customer = Customer::find($customer);

        // Trying to get property 'name' of non-object  - If we access not having id then it shows this error  , so to overcome this we are using below format

        // $customer = Customer::where('id',$customer)->firstOrFail();//HTTP ERROR 404 if id data not found

        // Eloquent Route Model Binding - Means 
        // When injecting a model ID to a route or controller action, you will often query the database to retrieve the model that corresponds to that ID. Laravel route model binding provides a convenient way to automatically inject the model instances directly into your routes. For example, instead of injecting a user's ID, you can inject the entire User model instance that matches the given ID.

        return view('customers.show',['customer'=>$customer]);
    }

    public function edit(Customer $customer){
        $company = Company::all();
        return view('customers.edit',['customer'=>$customer,'companylist'=>$company]);
    }
    public function update(Customer $customer){
       /*  $data = request()->validate([
            'name'=>'required | min:3',
            'email'=>'required | email',
            'active'=>'required',
            'company_id'=>'required'
        ]); */
        // $customer->update($data);
        $customer->update($this->validateRequest());
        

        return redirect('customers/'.$customer->id);

    }


    private function validateRequest(){
        return request()->validate([
            'name'=>'required | min:3',
            'email'=>'required | email',
            'active'=>'required',
            'company_id'=>'required'
            // 'random'=>''//optional field ; no validation
        ]);
    }


    public function destroy(Customer $customer){

        $customer->delete();

        return redirect('/customers');
    }


    /*
    * some extra stff
    * php artisan make:command AddCompanyCommand
    * protected $signature = 'contact:company';
    * protected $description = 'Adds A new Company';
    * php artisan contact:company
    * php artisan contact:company 'A New Company'

    */
    /*
    *Closure: Closure based commands : routes/console.php 
    php artisan inspire

    Company::whereDoesntHave('customers')
    ->get()
    ->each(function($company){
        $company->delete();
        $this->warn('Deleted '.$company->name);
    });
    */
    /* Model Factories: Fake data 
        Tinker - $c = \App\User::all()->pluck('name')
        factory(\App\User::class,3)->create();
        
        php artisan make:factory CompanyFactory -m Company
    */
    /* Database & Table Seeders:
        php artisan db:seed 
        php artisan make:seeder UsersTableSeeder

        factory(\App\Users::class,3)->create();  - use seeders or Tinker
        php artisan db:seed

        $this->call(UsersTableSeeder::class);
        ReflectionException  : Class CompaniesTableSeeder does not exist
        composer dump-autoload
        php artisan db:seed
    */

    /*
    Image Upload :
    Validate - Image is optional then use tap method , hasFile('image')
    $this->storeImage($customer);


    storage:link

    */

    /**
     * 
     * Telescope is providing a dashboard shows the request time limit and same same way , queries triggers all triggers will show herelike trihggering 
     * 
     * Lazy Loading & Eagar Loading ...?
     * 
     * use with('company')  it modify like comapny_id in (1,2,34,5,68,7,9,);
     * 
     * 
     * Policy
     */
}