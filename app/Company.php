<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //

    protected $guarded = [];
    //Using Tinker Sheelll
    // $c = App\Company::create(['name'=>'Infosys','phone'=>'123-456-789']);
    public function customers(){
        // return $this->hasMany(Customer::class,'id','company_id');
        return $this->hasMany(Customer::class);
    }
}
