@extends('layouts.app')
@section('content')
<h1>Contact US</h1>

<p>Lifecykul</p>

@if(!session()->has('message'))

<form action="{{route('contact.store')}}" method="post">

    @csrf
    <div class="form-group">
        <label for="">Name</label>
        <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control">
        <div>{{$errors->first('name')}}</div>
    </div>

    <div class="form-group">
        <label for="">Email</label>
        <input type="text" name="email" id="email" value="{{old('email')}}" class="form-control">
        <div>{{$errors->first('email')}}</div>

        <div class="form-group">
        <label for="">Message</label>
        <textarea name="message" id="" cols="10" rows="5" class="form-control"  value="{{old('message')}}"></textarea>
        <div>{{$errors->first('message')}}</div>


        <button type="submit" class="btn btn-primary">Send Message</button>

</form>

@endif


@endsection