@extends('layouts.app')

<!-- @section('title')
    Customrer Liast
@endsection -->

@section('title','Edit Details for '.$customer->name)



@section('content')

<!-- <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/about">About US</a></li>
    <li><a href="/contact">Contact Us</a></li>
    <li><a href="/customers">Customers List</a></li>
</ul> -->


<?php
// foreach($customers as $cust){
//     echo '<li>'.$cust.'</li>';
// }
?>

<div class="row">
    <div class="col-12">
        <h1>Edit details for Customers {{$customer->name}}</h1>

    </div>
</div>

<div class="row">
    <div class="col-12">
        <!-- customers post route -->
        <!-- <form action="/customers/{{$customer->id}}" method="post"> -->
        <form action="{{route('customers.update',['customer'=>$customer] )}}" method="post">
            @method('PATCH')
            @include('customers.form')
                <input type="submit" value="Edit" class="btn btn-primary">
            </div>

        </form>
    </div>
</div>







@endsection