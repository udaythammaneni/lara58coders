@extends('layouts.app')

<!-- @section('title')
    Customrer Liast
@endsection -->

@section('title','Customrer List')



@section('content')

<!-- <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/about">About US</a></li>
    <li><a href="/contact">Contact Us</a></li>
    <li><a href="/customers">Customers List</a></li>
</ul> -->


<?php
// foreach($customers as $cust){
//     echo '<li>'.$cust.'</li>';
// }
?>

<div class="row">
    <div class="col-12">
        <h1>Customers</h1>

        <p><a href="/customers/create">Add New Customer</a></p>

    </div>
</div>

@foreach($customers as $cust)
<div class="row">

        <div class="col-3"><a href="/customers/{{$cust->id}}">{{$cust->id}}</a></div>
        <div class="col-3">{{$cust->name}}</div>
        <div class="col-3">{{$cust->company->name}}</div>
        <div class="col-3">{{$cust->active}}</div>
        <!-- <div class="col-3">{{$cust->active == 1? 'Active' : 'In-Active'}}</div> -->
        <!-- Use Eloquent Accessors for Change Active & In-Active Text-->
        <!-- getActiveAttribute In Customer Model  -->
</div>
@endforeach


{{$customers->links()}}
<?php 
// <div class="row">

//     <div class="col-6">
//         <h3>Active Customers</h3>
//         <ul>
//             @foreach($actcustomers as $cust)
//             <li>{{$cust->name}} {{$cust->email}} {{$cust->company->name}}</li>
//             @endforeach
//         </ul>
//     </div>

//     <div class="col-6">
//         <h3>IN- Active Customers</h3>
//         <ul>
//             @foreach($inactcustomers as $cust)
//             <li>{{$cust->name}} {{$cust->email}} {{$cust->company->name}}</li>
//             @endforeach
//         </ul>
//     </div>
// </div>

?>


<div class="row">
    <div class="col-12">
        <?php 
        // @foreach($companylist as $company)
        // <h3>{{$company->name}}</h3>
        // <ul>
        //     @foreach($company->customers as $cust)
        //     <li> {{$cust->name}}</li>
        //     @endforeach
        // </ul>
        // @endforeach

        ?>

    </div>
</div>



@endsection