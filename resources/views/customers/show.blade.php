@extends('layouts.app')

<?php
//  @section('title')
//     Customrer Liast
// @endsection 
?>

@section('title','Details of '.$customer->name)



@section('content')

<!-- <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/about">About US</a></li>
    <li><a href="/contact">Contact Us</a></li>
    <li><a href="/customers">Customers List</a></li>
</ul> -->


<?php
// foreach($customers as $cust){
//     echo '<li>'.$cust.'</li>';
// }
?>

<div class="row">
    <div class="col-12">
        <h1>Details for {{$customer->name}}</h1>
        <p><a href="/customers/{{$customer->id}}/edit">Edit</a></p>


        <form action="/customers/{{$customer->id}}/" method="post">

        @method('DELETE')
        @csrf

        <button type="submit" class="btn btn-danger">Delete</button>
        </form>

    </div>
</div>

<div class="row">
    <div class="col-12">
        <p><strong>Name:</strong>{{$customer->name}}</p>
        <p><strong>Email:</strong>{{$customer->email}}</p>
        <p><strong>Company:</strong>{{$customer->company->name}}</p>
    </div>
</div>







@endsection