@csrf
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" name="name" id="name" value="{{old('name') ?? $customer->name}}" class="form-control">
                <div>{{$errors->first('name')}}</div>
            </div>

            <div class="form-group">
                <label for="">Email</label>
                <input type="text" name="email" id="email" value="{{old('email') ?? $customer->email}}" class="form-control">
                <div>{{$errors->first('email')}}</div>

                <div class="form-group">
                    <label for="company_id"></label>
                    <select name="company_id" id="company_id" class="form-control">
                        <option value="">Select Company</option>
                        @foreach($companylist as $comp)
                        <option value="{{$comp->id}}" {{$comp->id==$customer->company_id ? 'selected':''}}>{{$comp->name}}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Undefined index: check model attributes default -->
                <div class="form-group">
                    <label for="active"></label>
                    <select name="active" id="active" class="form-control">
                        <option value="">Select</option>
                        @foreach($customer->activeOptions() as $activeOptionsKey => $activeOptionsValue)
                        <option value="{{$activeOptionsKey}}" {{$customer->active==$activeOptionsValue ? 'selected' : ''}}>{{$activeOptionsValue}}</option>
                       
                        @endforeach
                        <!-- <option value="1" {{$customer->active=='Active' ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$customer->active=='Inactive' ? 'selected' : ''}}>Inactive</option> -->
                    </select>
                </div>